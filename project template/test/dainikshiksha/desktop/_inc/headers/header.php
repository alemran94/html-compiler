<!DOCTYPE html>
<html class="no-js" lang="en">

<head>
    <meta charset="utf-8">
    <meta name="description" content="desc">
    <meta name="author" content="Gurukul ICTDL">
    <meta name="viewport" content="width=device-width, initial-scale=1">



    <!-- seo -->

    <meta property="fb:app_id" content="108479729193136" />
    <meta property="fb:admins" content="684476602, 1269303818, 679476803, 549118759" />
    <meta property="fb:page_id" content="7138936668" />

    <meta property="twitter:account_id" content="1059801" />
    <meta name="twitter:card" content="app">
    <meta name="twitter:site" content="@dainikshiksha">
    <meta name="twitter:description" content="post excerpt">
    <meta name="twitter:app:country" content="BD">
    <meta name="twitter:image" content="assets/img/insideimage.jpg" />
    <meta name="twitter:creator" content="@dainikshiksha" />

    <meta property="og:locale" content="bn_BD" />
    <meta property="og:type" content="article" />
    <meta property="og:title" content="post title" />
    <meta property="og:description" content="post excerpt" />
    <meta property="og:url" content="http://dainikshiksha.com" />
    <meta property="og:site_name" content="Dainik Shiksha" />
    <meta property="og:updated_time" content="2012-12-05T15:03:52-05:00" />
    <meta property="og:image" content="assets/img/thumb.jpg" />
    <meta property="og:image" content="assets/img/insideimage.jpg" />
    <meta property="og:image" content="assets/img/insideimage.jpg" />
    <meta property="og:image" content="assets/img/insideimage.jpg" />



    <meta name="google-site-verification" content="c9app5XVp8lVmCyt-cHaPGOrTdwfrBbZW6HoZeH7Rnc" />

    <meta name="news_keywords" content="">


    <link rel="publisher" href="https://plus.google.com/103218677032751327334" />


    <script type="application/ld+json">
        {
            "@context": "http://schema.org",
            "@type": "Organization",
            "name": "Dainikshiksha",
            "url": "http://dainikshiksha.com",
            "contactPoint": {
                "@type": "ContactPoint",
                "telephone": "+1-401-555-1212",
                "contactType": "Customer service"
            },
            "sameAs": [
                "http://www.facebook.com/dainikshiksha",
                "http://www.twitter.com/dainikshiksha",
                "https://plus.google.com/+dainikshiksha"
            ]
        }
    </script>
    <!-- /seo -->


    <!-- Title -->
    <title>
        <?php echo PNAME . ' - ' . $pagetitle; ?>
    </title>
    <!-- Favicon and Touch Icons -->
    <link rel="shortcut icon" href="assets/img/favicon.png">
    <!-- Fonts -->
    <link href="https://fonts.maateen.me/siyam-rupali/font.css" rel="stylesheet">
    <link href="assets/css/icon-font.css" rel="stylesheet">
    <!-- Plugin CSS -->
    <link href="assets/css/bootstrap.css" rel="stylesheet">
    <link href="assets/css/simplelightbox.min.css" rel="stylesheet">

    <!-- Stylesheets -->
    <link href="assets/css/core.css" rel="stylesheet" media="screen">
    <link href="assets/css/responsive.css" rel="stylesheet" media="screen">

    <!-- JavaScript -->
    <!-- Core Library -->
    <script src="https://ajax.googleapis.com/ajax/libs/jquery/2.2.4/jquery.min.js"></script>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.12.9/umd/popper.min.js"></script>
    <script src="assets/js/bootstrap.min.js"></script>
    <!-- jQuery Plugins -->
    <script src="assets/js/simple-lightbox.min.js"></script>
    <script src="assets/js/fit.min.js"></script>
    <script src="assets/js/cross-datepicker.js"></script>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/vanilla-lazyload/10.4.1/lazyload.min.js"></script>
    <script src="assets/js/jquery.sharrre.min.js"></script>
    <!-- Custom Script -->
    <script src="assets/js/core.js"></script>

</head>

<body class=" <?php echo $klass; ?>">


    <!--  -->
    <!-- 
google analytics
fb code 
fb pixel
alexa
-->

    <div id="fb-root"></div>
    <script>
        (function (d, s, id) {
            var js, fjs = d.getElementsByTagName(s)[0];
            if (d.getElementById(id)) return;
            js = d.createElement(s);
            js.id = id;
            js.src = 'https://connect.facebook.net/bn_BD/sdk.js#xfbml=1&version=v2.11&appId=321677371587525';
            fjs.parentNode.insertBefore(js, fjs);
        }(document, 'script', 'facebook-jssdk'));
    </script>
    <!--  -->


    <header id="header">

        <!-- date/ social icons -->
        <div class="topbar container-fluid">
            <div class="dateupdate float-left">
                ঢাকা - শনিবার, ৩০ ডিসেম্বর ২০১৭, ১৬ পৌষ ১৪২৪, ১১ রবিউস সানি ১৪৩৯
            </div>
            <ul class="float-right list-inline mb-0">
                <li class="list-inline-item">
                    <a href="#" target="_blank">
                        <span class="fonticon-youtube"></span>
                    </a>
                </li>
                <li class="list-inline-item">
                    <a href="#" target="_blank">
                        <span class="fonticon-twitter"></span>
                    </a>
                </li>
                <li class="list-inline-item">
                    <a href="#" target="_blank">
                        <span class="fonticon-facebook"></span>
                    </a>
                </li>
                <li class="list-inline-item">
                    <a href="#" class="btn-x">পুরনো সংবাদ</a>
                </li>

            </ul>

            <div class="clearfix"></div>
        </div>

        <!-- ads -->
        <div class="ad-space ad-space-top">
            <div class="container-fluid">
                <div class="row">
                    <div class="col-3 text-left">
                        <?php get_part('others/ad', ['l'=>'ad.png'] ); ?>
                    </div>
                    <div class="col-3 text-center">
                        <?php get_part('others/ad', ['l'=>'ad.png'] ); ?>
                    </div>
                    <div class="col-3 text-center">
                        <?php get_part('others/ad', ['l'=>'ad.png'] ); ?>
                    </div>
                    <div class="col-3 text-right">
                        <?php get_part('others/ad', ['l'=>'ad.png'] ); ?>
                    </div>
                </div>
            </div>
        </div>

        <!-- hero -->
        <div class="container-fluid" id="hero">
            <div class="row">
                <div class="col-sm-8">
                    <img src="assets/img/vidyasagar-main.png" class="float-left mr-3" alt="dainikshiksha">
                    <small class="hero-info align-container">
                        <span class="align-middle">
                            ঈশ্বরচন্দ্র বিদ্যাসাগর (২৬ সেপ্টেম্বর ১৮২০ – ২৯ জুলাই ১৮৯১) উনবিংশ শতকের একজন বিশিষ্ট বাঙালি শিক্ষাবিদ, সমাজ সংস্কারক ও গদ্যকার।
                            তাঁর প্রকৃত নাম ঈশ্বরচন্দ্র বন্দ্যোপাধ্যায়।
                        </span>
                    </small>
                </div>
                <div class="col-sm-4 text-right align-container position-relative">
                    <a href="?" class="align-middle header-logo">
                        <img src="assets/img/dainikshiksha-logo.png" alt="dainikshiksha logo">
                    </a>
                </div>
            </div>
        </div>

        <nav class="navbar navbar-expand-lg navbar-light bg-light">
            <div class="container-fluid">
                <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarSupportedContent" aria-controls="navbarSupportedContent"
                    aria-expanded="false" aria-label="Toggle navigation">
                    <span class="navbar-toggler-icon"></span>
                </button>

                <div class="collapse navbar-collapse" id="navbarSupportedContent">
                    <ul class="navbar-nav mr-auto">
                        <li class="nav-item active">
                            <a class="nav-link" href="home.html">Home
                                <span class="sr-only">(current)</span>
                            </a>
                        </li>
                        <li class="nav-item">
                            <a class="nav-link" href="category.html">Category</a>
                        </li>
                        <li class="nav-item">
                            <a class="nav-link" href="featured.html">Leads</a>
                        </li>
                        <li class="nav-item">
                            <a class="nav-link" href="news.html">News</a>
                        </li>
                        <li class="nav-item">
                            <a class="nav-link" href="photo.html">Photo</a>
                        </li>
                        <li class="nav-item dropdown">
                            <a class="nav-link dropdown-toggle" href="#" id="navbarDropdown" role="button" data-toggle="dropdown" aria-haspopup="true"
                                aria-expanded="false">
                                Dropdown
                            </a>
                            <div class="dropdown-menu" aria-labelledby="navbarDropdown">
                                <a class="dropdown-item" href="#">Action</a>
                                <a class="dropdown-item" href="#">Another action</a>
                                <div class="dropdown-divider"></div>
                                <a class="dropdown-item" href="#">Something else here</a>
                            </div>
                        </li>
                    </ul>
                </div>
            </div>
        </nav>


    </header>