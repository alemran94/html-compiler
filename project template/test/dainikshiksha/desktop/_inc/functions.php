<?php 
define('PNAME', 'Dainikshiksha'); // project name
global $randtitles, $cats;
$randtitles = [
    'পরীক্ষার বদলে র‍্যাফেল ড্র করে নিয়োগ দেওয়া হোক',
    'কেকেকের কুখ্যাত খুনি এডগার রে মারা গেছেন',
    'নির্বাচনের বিষয়ে সংলাপের প্রয়োজন নেই',
    'আজ আখেরি মোনাজাত',
    'বিভ্রান্ত করতে নির্বাচনকালীন সরকার প্রসঙ্গ: মওদুদ',
    'বৈশ্বিক পুঁজিবাদের বিরুদ্ধে জয়লাভ করা সম্ভব'
];
$cats = ['শিক্ষা', 'এমপিও', 'বিশ্ববিদ্যালয়', 'মাদ্রাসা', 'অন্যান্য', 'কলেজ'];


/*
includes a template part (a php file) from _inc directory


$part		>> the file location (inside of _inc directory) we want to include without php extension
$klass		>> html class name(s), for quick usage. it's very usefull to section parts
$param		>> other parameters that might be needed, ex: breadcumb title, extra dynamic html contents etc. 
			>> we will pass data as an array with keys, ex: get_part('breadcumb', '', ['title' => 'about us', 'foo' => 'bar']);
			>> each of the keys will be trsnsformed as an indivisual variables for quick usages, ex: $title, $foo
			>> if we don't want to add any html classes but the parameters, we can use something like this too, cool nah?
			>> ex: get_part('breadcumb', ['title' => 'about us', 'foo' => 'bar']); 
*/
function get_part($part = '', $klass = '', $param = []){
	if($part != ''){
		if (is_array($klass)){
      $param = $klass;
      $klass = '';
		}
		extract($param);
		include($part.'.php');
	}
}

/*
this will make a slag from text
https://stackoverflow.com/questions/2955251/php-function-to-make-slug-url-string
*/
function slugify($text)
{
  // replace non letter or digits by -
  $text = preg_replace('~[^\pL\d]+~u', '-', $text);

  // transliterate
  $text = iconv('utf-8', 'us-ascii//TRANSLIT', $text);

  // remove unwanted characters
  $text = preg_replace('~[^-\w]+~', '', $text);

  // trim
  $text = trim($text, '-');

  // remove duplicate -
  $text = preg_replace('~-+~', '-', $text);

  // lowercase
  $text = strtolower($text);

  if (empty($text)) {
    return 'n-a';
  }

  return $text;
}

/*
same as get_part()
*/
function get_header($pagetitle = 'Home', $version = '', $klass = '', $param = []){
	$param['pagetitle'] = $pagetitle;
	$version			= ($version == '') ? $version : '-' . $version;

	//can be usefull if you need to add an extra class for each page according to it's page title
	$klass				= slugify($pagetitle) . ' ' . $klass; 
	
	get_part('headers/header'.$version, $klass, $param);
}

