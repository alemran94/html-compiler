<footer id="footer">
    <?php if(isset($isnews)): ?>
    <div class="clearfix pt-5"></div>
    <div class="container-fluid">
        <p class="h3 mb-3">বাছাইকৃত সংবাদঃ</p>
        <div class="row">
            <?php for($i=1; $i<=9; $i++): ?>
            <div class="col-4">
                <?php get_part('others/grid-vartical'); ?>
            </div>
            <?php endfor; ?>
        </div>
    </div>
    <?php endif; ?>

    <div class="clearfix pt-5"></div>
    <div class="footer-brand">
        <div class="container-fluid">
            <div class="row">
                <div class="col-4">
                    <img src="assets/img/dainikshiksha-logo-footer.png" alt="dainikshiksha" />
                </div>
                <div class="col-8 text-right">
                    <small>ডাটা সেভ করতে এখনি অ্যাপস ডাউনলোড করুন</small>
                    <a data-toggle="modal" data-target="#playstore" href="#">
                        <img src="assets/img/playstore.png" alt="dainikshiksha app">
                    </a>
                    <a data-toggle="modal" data-target="#playstore" href="#">
                        <img src="assets/img/appstore.png" alt="dainikshiksha app">
                    </a>
                </div>
            </div>
        </div>
    </div>
    <div class="footer-address">
        <div class="container-fluid">
            <div class="row">
                <div class="col-7">
                    <p class="pt-4">© সর্বস্বত্ব স্বত্বাধিকার সংরক্ষিত ২০১০-২০১৮
                        <br/>
                        <small>এই ওয়েবসাইটের কোনো লেখা বা ছবি অনুমতি ছাড়া নকল করা বা অন্য কোথাও প্রকাশ করা সম্পূর্ণ বেআইনি</small>
                    </p>
                    <p class="small">
                    Dainikshiksha.com is the most read Bangladeshi online newspaper and news agency based on education.<br/>
                        <a class="text-light" href="http://www.dainikshiksha.com/privacy-policy-dainikshiksha-mobile-app/">Privacy Policy – Dainikshiksha App</a>
                    </p>
                </div>
                <div class="col-5">
                    <div class="footer-address-content small">
                        <strong>সম্পাদকঃ</strong>
                        <span>সিদ্দিকুর রহমান খান </span>
                        <div class="clearfix"></div>
                        <strong>সম্পাদকীয় যোগাযোগঃ</strong>
                        <span>বাড়ী নং-১৩ (১ম তলা) নিউ ইস্কাটন, গাউসনগর, ঢাকা- ১০০০।
                            <br/>ফোন: +8802-9344440(নিউজ) 01757513287(নিউজ)</span>
                        <div class="clearfix"></div>
                        <strong>ই-মেইলঃ</strong>
                        <span>dainikshiksha@gmail.com (নিউজ)</span>
                        <div class="clearfix"></div>
                        <strong>বিজ্ঞাপনঃ</strong>
                        <span>বিকাশ- 01757514061, ই-মেইল- ad.dainikshiksha@gmail.com</span>
                        <div class="clearfix"></div>
                        <div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>





    <!-- extra -->
    <div id="extra">
        <!-- modals -->

        <!-- subscribe -->
        <div class="modal fade appdownloadmodal" id="subscribe" tabindex="-1" role="dialog" aria-hidden="true">
            <div class="modal-dialog" role="document">
                <div class="modal-content">
                    <div class="modal-header">
                        <h5 class="modal-title text-center" id="subscribeModalLongTitle">দৈনিকশিক্ষা সাবস্ক্রাইব ফর্ম</h5>
                        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                            <span aria-hidden="true">&times;</span>
                        </button>
                    </div>
                    <div class="modal-body">
            
                    </div>
                </div>
            </div>
        </div>


        <!-- app qr -->
        <div class="modal fade appdownloadmodal" id="playstore" tabindex="-1" role="dialog" aria-hidden="true">
            <div class="modal-dialog" role="document">
                <div class="modal-content">
                    <div class="modal-header">
                        <h5 class="modal-title text-center" id="playstoreModalLongTitle">দৈনিকশিক্ষা মোবাইল অ্যাপ</h5>
                        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                            <span aria-hidden="true">&times;</span>
                        </button>
                    </div>
                    <div class="modal-body">
                        <img class="float-left" src='https://chart.googleapis.com/chart?cht=qr&chl=https%3A%2F%2Fplay.google.com%2Fstore%2Fapps%2Fdetails%3Fid%3Dcom.dainikshiksha.app.android&chs=250x250&choe=UTF-8&chld=L|2'
                            alt=''>
                        <a href="https://play.google.com/store/apps/details?id=com.dainikshiksha.app.android" target="_blank" class="btn btn-lg btn-dark">
                            Google Playstore থেকে ডাউনলোড করুন।
                        </a>
                        <!-- <p class="small">

                </p> -->
                    </div>
                </div>
            </div>
        </div>


        <div id="reveal-on-scroll">
            <!-- fb container -->
            <div id="fb" class="card">
                <div class="fb-box">
                    <div class="fb-page" data-href="https://www.facebook.com/dainikshiksha/" data-tabs="timeline, messages" data-height="400px"
                        data-small-header="false" data-adapt-container-width="true" data-hide-cover="false" data-show-facepile="true">
                        <blockquote cite="https://www.facebook.com/dainikshiksha/" class="fb-xfbml-parse-ignore">
                            <a href="https://www.facebook.com/dainikshiksha/">DainikShiksha</a>
                        </blockquote>
                    </div>
                </div>
                <img src='assets/img/facebook.png' class="fb-opener" />
                <a href="#" class="fb-closer">
                    Close
                </a>
            </div>

            <!-- scroll to top -->
            <div id="scrolltotop">
                <a class="arrow-up">
                    <span class="left-arm"></span>
                    <span class="right-arm"></span>
                </a>
            </div>
        </div>


        <!-- scrollbar -->




    </div>
</footer>
<?php get_part('others/scrollbar'); ?>

<Script>
        var myLazyLoad = new LazyLoad();
</Script>
</body>

</html>