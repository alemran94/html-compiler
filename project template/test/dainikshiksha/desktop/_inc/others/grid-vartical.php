<?php global $randtitles; $n = rand(0,5); ?>
<a class="media card grid grid-vartical <?php echo $klass; ?>" href="news.html">
        <?php if(!isset($noimage)): ?>
            <img src="assets/img/post-thumb/0.jpg" data-src="assets/img/post-thumb/<?php echo $n +1 ;?>.jpg" alt="Card image cap">
        <?php endif; ?>
        <div class="media-body">
            <h5 class="mt-0 mb-0"><?php echo $randtitles[$n]; ?></h5>
        </div>
</a>