<aside id="sidebar">
    <div class="widget mb-0 widget-search">
        <form action="" method="GET" id="search-form">
            <div class="form-group">
                <input type="text" class="form-control form-control-sm" id="search-input" name="search" placeholder="কিছু খুঁজতে চান? ">
            </div>
            <div class="form-group">
                <input type="date" class="form-control form-control-sm" id="search-date" name="date">
                <small id="datehelp" class="sr-only form-text text-muted">পুরনো সংবাদ খুঁজতে তারিখ নির্বাচন করুন</small>
            </div>
            <div class="form-group text-right">
                <button type="submit" class="btn btn-sm controls btn-primary">খুঁজুন</button>
            </div>
        </form>
    </div>

    <div class="widget widget-ad-space">
        <?php get_part('others/ad', ['l'=>'ad.png'] ); ?>
    </div>


    <div class="widget widget-latest">
        <h3 class="widget-header">
            সর্বশেষ সংবাদ
        </h3>
        <div class="widget-body">
            <ul class="list-unstyled">
                <?php
                    global $randtitles;
                    for($i=1; $i<=10; $i++):  $n = rand(0,5); ?>
                    <li class="media mb-4">

                        <img class="mr-3 mini-thumb" src="assets/img/post-thumb/<?php echo $n +1 ;?>.jpg" alt="Generic placeholder image">
                        <div class="media-body">
                            <a href="#">
                                <?php echo $randtitles[$n]; ?>
                            </a>
                        </div>

                    </li>
                    <?php endfor; ?>
            </ul>
        </div>
    </div>


    <div class="widget opinion">
        <h3 class="widget-header">
            শিক্ষাবিদের কলাম
        </h3>
        <div class="widget-body">
            <ul class="list-unstyled">
                <li class="media mb-4">
                    <img class="mr-3" src="assets/img/siddiqu-sir-100x100.jpg" alt="Generic placeholder image">
                    <div class="media-body">
                        <h5 class="mt-0 mb-1">মো. সিদ্দিকুর রহমান</h5>
                        <a class="d-block text-truncate" href="#">জ্ঞান অর্জন হোক শিক্ষার মূল লক্ষ্য</a>
                    </div>
                </li>
                <li class="media mb-4">
                    <img class="mr-3" src="assets/img/Mujammel-Ali-100x100.jpg" alt="Generic placeholder image">
                    <div class="media-body">
                        <h5 class="mt-0 mb-1">অধ্যক্ষ মুজম্মিল আলী</h5>
                        <a class="d-block text-truncate" href="#">শেষ ভাল তো সবই ভালশেষ ভাল তো সবই ভাল</a>
                    </div>
                </li>
                <li class="media mb-4">
                    <img class="mr-3" src="assets/img/siddiqu-sir-100x100.jpg" alt="Generic placeholder image">
                    <div class="media-body">
                        <h5 class="mt-0 mb-1">মো. সিদ্দিকুর রহমান</h5>
                        <a class="d-block text-truncate" href="#">জ্ঞান অর্জন হোক শিক্ষার মূল লক্ষ্য</a>
                    </div>
                </li>
                <li class="media mb-4">
                    <img class="mr-3" src="assets/img/Mujammel-Ali-100x100.jpg" alt="Generic placeholder image">
                    <div class="media-body">
                        <h5 class="mt-0 mb-1">অধ্যক্ষ মুজম্মিল আলী</h5>
                        <a class="d-block text-truncate" href="#">শেষ ভাল তো সবই ভাল</a>
                    </div>
                </li>
                <li class="media mb-4">
                    <img class="mr-3" src="assets/img/siddiqu-sir-100x100.jpg" alt="Generic placeholder image">
                    <div class="media-body">
                        <h5 class="mt-0 mb-1">মো. সিদ্দিকুর রহমান</h5>
                        <a class="d-block text-truncate" href="#">জ্ঞান অর্জন হোক শিক্ষার মূল লক্ষ্য</a>
                    </div>
                </li>
            </ul>
        </div>
    </div>

    <!-- <div class="widget widget-ad-space">
        <?php get_part('others/ad', ['l'=>'ad.png'] ); ?>
    </div>
 -->
    <div class="widget widget-ad-space">
        <?php get_part('others/ad', ['l'=>'ad-tall.png'] ); ?>
    </div>

</aside>