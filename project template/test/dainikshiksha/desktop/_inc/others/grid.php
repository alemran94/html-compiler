<?php global $randtitles; $n = rand(0,5); ?>
<a class="card grid <?php echo $klass; ?>" href="news.html">
    <img class="card-img-top" data-src="assets/img/post-thumb/<?php echo $n +1 ;?>.jpg" src="assets/img/post-thumb/0.jpg" alt="Card image cap">
    <div class="card-body">
        <h5 class="card-title">
            <?php echo $randtitles[$n]; ?>
        </h5>

        <?php if(isset($hidedesc)):?>
        <p class="card-text cat-name">
            <?php global $cats; echo $cats[rand(0, 5)]; ?>
        <?php else: ?>
        <p class="card-text">দেশের বাজারে সোনার দর ভরিতে সর্বোচ্চ ১ হাজার ৪০০ টাকা পর্যন্ত বাড়ানোর সিদ্ধান্ত নিয়েছে বাংলাদেশ জুয়েলার্স সমিতি।
            এতে ভালো মানের বা ২২ ক্যারেট সোনার ভরির দাম দাঁড়াবে ৫০ হাজার ৭৩৮ টাকা। সোনার নতুন দর কাল বুধবার থেকে সারা
            দেশে কার্যকর হবে।
        <?php endif;?>
        </p>
    </div>
</a>