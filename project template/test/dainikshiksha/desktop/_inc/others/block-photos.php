<div class="card block-photos">
    <div class="card-body">
        <div class="row">
            <div class="col-7">
                <a href="photo.html" class="grid-photo">
                    <img  src="assets/img/post-thumb/0.jpg" data-src="assets/img/post-thumb/7.jpg" alt="" />
                    <p class="caption">
                    ডেট্রয়েটে অটো শো
                    </p>
                </a>
                <a href="photo.html" class="grid-photo">
                    <img  src="assets/img/post-thumb/0.jpg" data-src="assets/img/post-thumb/8.jpg" alt="" />
                    <p class="caption">
                    রঙে রঙিন সরস্বতী
                    </p>
                </a>
            </div>
            <div class="col-5 text-center">
                <?php for($i=1; $i<=3; $i++): ?>
                    <a href="photo.html" class="grid-photo thumb">
                        <img  src="assets/img/post-thumb/0.jpg" data-src="assets/img/post-thumb/8.jpg" alt="" />
                        <p class="caption">
                        রঙে রঙিন সরস্বতী
                        </p>
                    </a>
                <?php endfor; ?>
            </div>
        </div>
    </div>
</div>