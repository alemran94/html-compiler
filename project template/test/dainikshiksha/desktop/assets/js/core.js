jQuery(document).ready(function ($) {
    "use strict";


    $(".ad-container").each(function () {
        var p = $(this).get(0);
        var c = $(this).find('.ad').get(0);
        // fit bar into foo
        // the third options argument is optional, see the README for defaults
        // https://github.com/soulwire/fit.js
        fit(c, p, {

            // Alignment
            hAlign: fit.CENTER, // or fit.LEFT, fit.RIGHT
            vAlign: fit.CENTER, // or fit.TOP, fit.BOTTOM

            // Fit within the area or fill it all (true)
            cover: false,

            // Fit again automatically on window resize
            watch: true,

            // Apply computed transformations (true) or just
            // return the transformation definition (false)
            apply: true
        });

    });

    // date picker
    $("input[type='date']").cdp();





    var urlCurlSharrre = 'http://localhost/panel/products/html/dainikshiksha/desktop/sharrre.php';
    $('#twitter-before-261').sharrre({
        share: {
            twitter: true
        },
        urlCurl: urlCurlSharrre,
        enableHover: false,
        enableTracking: true,
        buttons: {
            twitter: {
                via: 'My-Blog'
            }
        },
        click: function (api, options) {
            api.simulateClick();
            api.openPopup('twitter');
        }
    });
    $('#googleplus-before-261').sharrre({
        share: {
            googlePlus: true
        },
        urlCurl: urlCurlSharrre,
        enableHover: false,
        enableTracking: true,
        buttons: {},
        click: function (api, options) {
            api.simulateClick();
            api.openPopup('googlePlus');
        }
    });
    $('#facebook-before-261').sharrre({
        share: {
            facebook: true
        },
        urlCurl: urlCurlSharrre,
        enableHover: false,
        enableTracking: true,
        buttons: {},
        click: function (api, options) {
            api.simulateClick();
            api.openPopup('facebook');
        }
    });
    $('#linkedin-before-261').sharrre({
        share: {
            linkedin: true
        },
        urlCurl: urlCurlSharrre,
        enableHover: false,
        enableTracking: true,
        buttons: {},
        click: function (api, options) {
            api.simulateClick();
            api.openPopup('linkedin');
        }
    });
    $('#pinterest-before-261').sharrre({
        share: {
            pinterest: true
        },
        urlCurl: urlCurlSharrre,
        enableHover: false,
        enableTracking: true,
        buttons: {},
        click: function (api, options) {
            api.simulateClick();
            api.openPopup('pinterest');
        }
    });

    $('.fb-opener').on('click', function (e) {
        e.preventDefault();
        $('#fb').addClass('active');
    });

    $('.fb-closer').on('click', function (e) {
        e.preventDefault();
        $('#fb').removeClass('active');
    });

    var gallery = $('.lightbox').simpleLightbox();




    // scroll
    $(window).scroll(function () {
        var scrolltop = $(this).scrollTop();
        if (scrolltop > 400) {
            $('#reveal-on-scroll').fadeIn(300);
        } else {
            $('#reveal-on-scroll').fadeOut(300);
        }
        if (scrolltop > 200) {
            $('#bottom-bar').addClass('show');
        } else {
            $('#bottom-bar').removeClass('show');
        }
    });
    $('#scrolltotop').on('click', function () {
        $("html, body").animate({
            scrollTop: 0
        }, 500);
        return false;
    });

});


//
function resizeText(multiplier) {
    var content = document.getElementById("entry-content");
    if (content.style.fontSize === "") {
        content.style.fontSize = "1.0em";
    }
    content.style.fontSize = parseFloat(content.style.fontSize) + (multiplier * 0.2) + "em";
}