<?php 
include('lib/helper.php');
include('lib/imgBlur.php');
include('lib/coreClass.php');



$dir = $argv[1];
$c = new CoreCompile($dir);
d('--------');

$c->copyFiles('assets');
d('--------');

d('Compiling html');
foreach(glob($dir . "\*.html") as $file) {
    $c->makeHTML($file);
}
d('--------');




if(isset($argv[3]) && $argv[2] == 'blur'){
	$exclude = [];
	$fn = readline("Want to exclude any images or continue? \n  ");

	while(1){
		if($fn == 'continue'){
			break;
		}else{
			$exclude[] = trim($fn);
			$fn = readline("  ");
		}
	}
		
	
	d('Starting magick engine');
	$c->makeBlur('assets/' . $argv[3], $exclude);
}

d("\n\n-----------------------------------\nYosh! Task completed!\n-----------------------------------");
