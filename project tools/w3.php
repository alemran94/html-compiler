<?php 
include('lib/helper.php');

function do_validate($f){
$ch = curl_init();
$file = file_get_contents($f);
curl_setopt($ch, CURLOPT_URL,"https://validator.w3.org/nu/?out=json");
curl_setopt($ch, CURLOPT_POST, 1);
curl_setopt($ch, CURLOPT_POSTFIELDS,$file);  //Post Fields
curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);

$headers = [
    'Content-Type: text/html; charset=UTF-8',
    'User-Agent: Mozilla/5.0 (X11; Linux x86_64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/41.0.2272.101 Safari/537.36',
    'X-MicrosoftAjax: Delta=true'
];

curl_setopt($ch, CURLOPT_HTTPHEADER, $headers);

$o = json_decode(curl_exec ($ch));

curl_close ($ch);

$e = 0; $w = 0; $i = 0;
foreach($o->messages as $m){
	if($m->type == 'error') $e = $e + 1; 
	if($m->type == 'warning') $w = $w + 1; 
	if($m->type == 'info') $i = $i + 1; 
}
return "Info:$i; Warning:$w; Error:$e;";
}




$dir = $argv[1];

foreach(glob($dir . "\*.html") as $file) {
    echo "Checking " . basename($file);
	echo ' ...... ' . do_validate($file);
	echo "\n";

}
d('--------');