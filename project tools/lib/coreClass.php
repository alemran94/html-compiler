<?php 


class CoreCompile{
	
	function __construct($dir) {
        $this->inputDir = $dir;
		$this->parentDir = dirname( $dir, 1 );
		$this->projectName = basename($dir);
		$this->projectDir = $this->parentDir . '/_' . $this->projectName;
		$this->supportedFormat = array('gif','jpg','jpeg','png');
		//$this->supportedFormat = array('png');
		
		
		if(!is_dir($this->projectDir)){
			//Directory does not exist, so lets create it.
			d("Creating project directory - " . $this->projectDir);
			mkdir($this->projectDir, 0755, true);
			d('done!');
		}else{
			d("Project directory already exists. Failed");
			exit();
		}
    }

	
	private function xcopy($source, $dest, $permissions = 0755){

		if (is_link($source)) {
			return symlink(readlink($source), $dest);
		}


		if (is_file($source)) {
			return copy($source, $dest);
		}


		if (!is_dir($dest)) {
			mkdir($dest, $permissions);
		}

		// Loop through the folder
		$dir = dir($source);
		while (false !== $entry = $dir->read()) {
			// Skip pointers
			if ($entry == '.' || $entry == '..') {
				continue;
			}

			// Deep copy directories
			$this->xcopy("$source/$entry", "$dest/$entry", $permissions);
		}

		// Clean up
		$dir->close();
		return true;
	}
	
	private function delete_directory($dirname) {
			 if (is_dir($dirname))
			   $dir_handle = opendir($dirname);
		 if (!$dir_handle)
			  return false;
		 while($file = readdir($dir_handle)) {
			   if ($file != "." && $file != "..") {
					if (!is_dir($dirname."/".$file))
						 unlink($dirname."/".$file);
					else
						 delete_directory($dirname.'/'.$file);
			   }
		 }
		 closedir($dir_handle);
		 rmdir($dirname);
		 return true;
	}	
	
	public function makeHTML($f){
		$fn = basename($f);
	
		ob_start();
			include($f);
			$out1 = ob_get_contents();
		ob_end_clean();
		
		file_put_contents($this->projectDir .'/'. $fn, $out1);
		d('Compiled: ' . $fn);
	}
	
	public function copyFiles($source){
		$dest = $this->projectDir . '/' . $source;
		$source = $this->inputDir . '/' . $source;
		
		d("Copying $source");
		$this->xcopy($source, $dest);
		d("done!");
		
		
	}
	
	public function makeBlur($imgDir, $exclude){
		$imgDir = $this->projectDir . '/' . $imgDir;
		doBlur($imgDir, $exclude, $this->supportedFormat);
	}
	
	
}
