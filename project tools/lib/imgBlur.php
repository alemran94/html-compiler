<?php

function doBlur($imgDir, $exclude, $supportedFormat){
	foreach (new RecursiveIteratorIterator(new RecursiveDirectoryIterator($imgDir)) as $f){

		$ext = strtolower(pathinfo($f, PATHINFO_EXTENSION));
		if (in_array($ext, $supportedFormat)){
			
			if(!in_array(basename($f), $exclude)){
				list($width, $height, $type, $attr) = getimagesize($f);
				exec("magick convert ".$f." -blur 0x8 -gravity south -annotate 0 ".$width."x".$height." ".$f);

				
				/*
				$img = new Imagick($f);
				$img->adaptiveBlurImage(0,8);
				
				$img->writeImage ($f);
				*/
				echo "Blured on 0x8: $f \n";
			}
			
		} else {
			continue;
		}
	}
}