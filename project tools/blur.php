<?php 
include('lib/helper.php');
include('lib/imgBlur.php');

$dir = $argv[1];
$supportedFormat = array('gif','jpg','jpeg','png');

$exclude = [];
$fn = readline("Want to exclude any images or continue? \n  ");

while(1){
	if($fn == 'continue'){
		break;
	}else{
		$exclude[] = trim($fn);
		$fn = readline("  ");
	}
}
	

doBlur($argv[1], $exclude, $supportedFormat);


d("\n\n-----------------------------------\nYosh! Task completed!\n-----------------------------------");
